package com.tiw;

import java.util.InputMismatchException;
import java.util.Scanner;

/** AppXO */
public class AppXO {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    char[][] board = {
      {' ', ' ', ' '},
      {' ', ' ', ' '},
      {' ', ' ', ' '},
    };

    System.out.println("Welcome to XO Game!!");
    char currentPlay = 'X';
    while (true) {
      showBoard(board);
      PlayTurn(board, scanner, currentPlay);
      if (checkWinner(board, currentPlay)) {
        showBoard(board);
        System.out.println("Player " + currentPlay + " wins!");
        break;
      }
      if (isBoardFull(board)) {
        showBoard(board);
        System.out.println("The game is a tie!");
        break;
      }
      if (currentPlay == 'X') {
        currentPlay = 'O';
      } else {
        currentPlay = 'X';
      }
    }
  }

  private static void showBoard(char[][] board) {
    System.out.println(board[0][0] + "|" + board[0][1] + "|" + board[0][2]);
    System.out.println("-+-+-");
    System.out.println(board[1][0] + "|" + board[1][1] + "|" + board[1][2]);
    System.out.println("-+-+-");
    System.out.println(board[2][0] + "|" + board[2][1] + "|" + board[2][2]);
    System.out.println();
  }

  private static void PlayTurn(char[][] board, Scanner scanner, char currentPlay) {

    while (true) {
      try {
        System.out.printf("Turn " + currentPlay + "\nPlease input (row, col): ");
        int row = scanner.nextInt() - 1;
        int col = scanner.nextInt() - 1;
        if (row < 0 || row >= 3 || col < 0 || col >= 3) {
          System.out.println("Invalid position. Try again.");
        } else if (board[row][col] != ' ') {
          System.out.println("Position already taken. Try again.");
        } else {
          board[row][col] = currentPlay;
          break;
        }
      } catch (InputMismatchException e) {
        System.out.println("Please enter integers for row and col. Try again.");
        scanner.nextLine();
      }
    }
  }

  private static boolean checkWinner(char[][] board, char currentPlay) {
    for (int i = 0; i < 3; i++) {
      if (board[i][0] == currentPlay && board[i][1] == currentPlay && board[i][2] == currentPlay)
        return true;
      if (board[0][i] == currentPlay && board[1][i] == currentPlay && board[2][i] == currentPlay)
        return true;
    }
    if (board[0][0] == currentPlay && board[1][1] == currentPlay && board[2][2] == currentPlay)
      return true;
    if (board[0][2] == currentPlay && board[1][1] == currentPlay && board[2][0] == currentPlay)
      return true;
    return false;
  }

  private static boolean isBoardFull(char[][] board) {
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        if (board[i][j] == ' ') {
          return false;
        }
      }
    }
    return true;
  }
}
